<?php

namespace App;

use Carbon\Carbon;
use App\models\Helpers;
use App\Mail\AccountActivation;
use App\models\PasswordResetCode;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'username', 'email', 'password', 'last_password_reset'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_key', 'last_password_reset'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * eloquent relationships
     */
    public function passwordResetCodes()
    {
        return $this->hasMany(PasswordResetCode::class);
    }

    /**
     * model functions
     */
    public function setActivationkeyAttribute($value)
    {
        Mail::to($this->email)->send(new AccountActivation($value, $this->username));
        $this->attributes['activation_key'] = Hash::make($value);
    }

    public function generateActivationKey()
    {
        $this->activation_key = Helpers::randomNumber();
        $this->save();
    }

    public function activate()
    {
        $this->activated = true;
        $this->save();
    }

    public function canResetPasswordNow()
    {
        if(Carbon::now()->diffInHours(Carbon::parse($this->last_password_reset)) >= 24) {
            return true;
        }
        return false;
    }

    public function deactivateOTPCodes()
    {
        $this->passwordResetCodes()->update(['expired' => true]);
    }

    public function validCode($code)
    {
        $codeInst = $this->passwordResetCodes()->where('code', $code)->first();
        if(!is_null($codeInst) && !$codeInst->expired) {
            $codeInst->update(['expired' => true]);
            return true;
        }
        return false;
    }
}
