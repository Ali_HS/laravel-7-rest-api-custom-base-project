<?php

namespace App\models;

use Illuminate\Support\Str;

class Helpers
{
    public static function randomNumber()
    {
        $digits = env('ACTIVATION_KEYS_LENGTH');
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    public static function randomString()
    {
        return Str::random(env('RANDOM_STRINGS_LENGTH'));
    }
}
