<?php

namespace App\models;

class ResponseBuilder
{
    public const _SUCCESS = true;
    public const _FAIL = false;

    private $success;
    private $payload;
    private $code;
    private $messages;
    private $response;

    public static function build($success)
    {
        $response = new ResponseBuilder();
        $response->success = is_bool($success) ? $success : true;
        return $response;
    }

    public function payload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    public function code($code)
    {
        $this->code = $code;
        return $this;
    }

    public function messages($messages)
    {
        $this->messages = array();
        if(!is_object($messages)) {
            $this->messages['custom_error_message'] = $messages;
        } else {
            $this->messages['validation_error_messages'] = $messages;
        }
        return $this;
    }

    public function return()
    {
        $this->response = [];
        $this->response['success'] = $this->success;
        if($this->success) {
            $this->response['payload'] = $this->payload;
        } else {
            $this->response['messages'] = $this->messages;
        }
        return response()->json($this->response, $this->code);
    }
}
