<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\models\Helpers;
use App\Mail\PasswordReset;
use App\models\Mailer;
use Illuminate\Http\Request;
use App\models\ResponseBuilder;
use App\Rules\MustContainDigits;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Rules\MustContainLowercaseLetters;
use App\Rules\MustContainUppercaseLetters;
use Symfony\Component\HttpFoundation\Response;

class CustomAuthController extends Controller
{
    /**
     * Registers a new user in the application
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function register(Request $request) {

        $this->validator = Validator::make($request->all(), [
            'full_name' => 'required|string|min:3|max:64',
            'username' => 'required|string|min:3|max:64',
            'email' => 'required|email|unique:users,email',
            'password' => [
                'required',
                'confirmed',
                'min:8',
                new MustContainDigits(),
                new MustContainUppercaseLetters(),
                new MustContainLowercaseLetters()
            ],
            'device_name' => 'required|string'
        ]);

        if($this->validator->fails()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages($this->validator->errors())
            ->code(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->return();
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['last_password_reset'] = Carbon::parse(now())->format(env('DEFAULT_DATETIME_FORMAT'));
        unset($input['device_name']);

        $user = User::create($input);
        $user->generateActivationKey();
        $user['api_token'] = $user->createToken($request->device_name)->plainTextToken;

        return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
        ->payload($user)
        ->code(Response::HTTP_CREATED)
        ->return();
    }

    /**
     * logs a user into the application
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function login(Request $request) {

        // Mailer::prepare();
        // $mailer = new Mailer();
        // $mailer->fromEmail('support@template.com')
        //     ->fromName('Template Support')
        //     ->toEmail('ali.habieb.97@hotmail.com')
        //     ->toName('Ali Habieb')
        //     ->message();
        // Mailer::send();
        // dd('sent');

        $this->validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required|string'
        ]);

        if($this->validator->fails()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages($this->validator->errors())
            ->code(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->return();
        }

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages('the provided credentials are invalid')
            ->code(Response::HTTP_BAD_REQUEST)
            ->return();
        }

        $user['api_token'] = $user->createToken($request->device_name)->plainTextToken;

        return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
        ->payload($user)
        ->code(Response::HTTP_OK)
        ->return();
    }

    /**
     * logs a user out of the the application by destroying current token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function logout(Request $request) {
        $user = $request->user();

        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();

        return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
        ->payload($user)
        ->code(Response::HTTP_OK)
        ->return();
    }

    /**
     * logs a user out of the the application by destroying all tokens
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function logoutAll(Request $request) {
        $user = $request->user();

        $user->tokens()->delete();

        return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
        ->payload($user)
        ->code(Response::HTTP_OK)
        ->return();
    }

    /**
     * activating a user account by verifying activation key
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function activateAccount(Request $request) {
        $length = env('ACTIVATION_KEYS_LENGTH');

        $this->validator = Validator::make($request->all(), [
            'activation_key' => 'required|numeric|min:'.$length.'|max:'.$length,
        ]);

        if($this->validator->fails()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages($this->validator->errors())
            ->code(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->return();
        }

        $user = $request->user();

        if(Hash::check($request->activation_key, $user->activation_key)) {
            $user->activate();
            return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
            ->payload($user)
            ->code(Response::HTTP_OK)
            ->return();
        }

        return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages('operation failed')
            ->code(Response::HTTP_NOT_ACCEPTABLE)
            ->return();
    }

    /**
     * resets a user password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function resetPassword(Request $request)
    {
        $user = $request->user();

        if(!$user->canResetPasswordNow()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages('cannot reset password before 24 hours after last reset')
            ->code(Response::HTTP_SERVICE_UNAVAILABLE)
            ->return();
        }

        $this->validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => [
                'required',
                'confirmed',
                'min:8',
                new MustContainDigits(),
                new MustContainUppercaseLetters(),
                new MustContainLowercaseLetters()
            ],
        ]);

        if($this->validator->fails()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages($this->validator->errors())
            ->code(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->return();
        }

        if (!Hash::check($request->old_password, $user->password)) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages('the provided password is invalid')
            ->code(Response::HTTP_BAD_REQUEST)
            ->return();
        }

        $user->password = Hash::make($request->new_password);
        $user->last_password_reset = Carbon::parse(now())->format(env('DEFAULT_DATETIME_FORMAT'));
        $user->save();

        return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
        ->payload($user)
        ->code(Response::HTTP_OK)
        ->return();
    }

    /**
     * resets a user password by sending the new password to the user email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\models\ResponseBuilder
     */
    public function forgotPassword(Request $request)
    {
        $this->validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($this->validator->fails()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages($this->validator->errors())
            ->code(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->return();
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages('cannot reset password for provided email')
            ->code(Response::HTTP_BAD_REQUEST)
            ->return();
        } elseif (!$user->canResetPasswordNow()) {
            return ResponseBuilder::build(ResponseBuilder::_FAIL)
            ->messages('cannot reset password before 24 hours after last reset')
            ->code(Response::HTTP_SERVICE_UNAVAILABLE)
            ->return();
        }

        $newPassword = Helpers::randomString();
        $user->password = Hash::make($newPassword);
        $user->last_password_reset = Carbon::parse(now())->format(env('DEFAULT_DATETIME_FORMAT'));
        $user->save();

        Mail::to($request->email)->send(new PasswordReset($user->username, $newPassword));

        return ResponseBuilder::build(ResponseBuilder::_SUCCESS)
        ->payload($user)
        ->code(Response::HTTP_OK)
        ->return();
    }
}
