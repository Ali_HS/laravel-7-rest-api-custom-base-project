@component('mail::message')
# Activate Your Account
Dear {{ $username }},<br>
{{ $code }} is your {{ env('app.name') }} account activation code.

{{-- @component('mail::button', ['url' => ''])
Confirm Your Email
@endcomponent --}}

Thanks,<br>
{{ env('app.name') }}
@endcomponent
