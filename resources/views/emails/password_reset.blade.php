@component('mail::message')
# Your password has been reset
Dear {{ $username }},<br>
{{ $password }} is your new {{ env('app.name') }} account password,<br>
you can reset it again soon in 24 hours.

{{-- @component('mail::button', ['url' => ''])
Confirm Your Email
@endcomponent --}}

Thanks,<br>
{{ env('app.name') }}
@endcomponent
